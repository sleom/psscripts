﻿param([string]$server = $(Throw "Parameter Missing: -server 'sql server name'"),
    [string]$user = $(Throw "Parameter Missing: -user 'username'"),
    [string]$passwd = $(Throw "Parameter Missing: -passwd 'username'"))

trap{
	Write-Host "Error found!" -ForegroundColor Red
	Write-Host "Variables dump:" -BackgroundColor DarkRed
	Get-Variable -Scope "Script"
	Write-Host "Variables dump end" -BackgroundColor DarkRed
    $c=Get-Variable -Name "connection" -ValueOnly
    #Write-Host $c
    if($c -ne $null){
        ($c -as [System.Data.SqlClient.SqlConnection]).Dispose();
    }
    Write-Error $_.Exception;
	exit 150;
}

$cmdText = "EXEC [dbo].[usp_CleanCacheData]"

$builder = New-Object System.Data.SqlClient.SqlConnectionStringBuilder;
$builder.psbase.DataSource = "$server";
$builder.psbase.UserID = "$user";
$builder.psbase.Password = "$passwd";
$builder.psbase.InitialCatalog = "Maintenance"
Write-Host $builder.ConnectionString
#exit 0;
$connection = New-Object System.Data.SqlClient.SqlConnection($builder.ConnectionString);
$cmd = New-Object System.Data.SqlClient.SqlCommand($cmdText, $connection);
$connection.Open();
$res = $cmd.ExecuteNonQuery();
$connection.Dispose();
if($res -ne -1)
{
    exit 100;
}
exit 0;