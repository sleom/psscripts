﻿param([string]$Name = $(Throw "Parameter Missing: -Name 'site name'"),
        [string]$rootPath = "C:\inetpub\wwwroot\",
        [string]$deploymentUserName = "teg.loc\hktegdeploy")

trap{
    break
}

$poolName = $Name.Split('.', [System.StringSplitOptions]::RemoveEmptyEntries)[0]
$sitePath = "$rootPath$Name"



# test & create physical path

#CD D:\

if(!(Test-Path $sitePath -PathType Container)){
    New-Item -Path $sitePath -ItemType directory
}

Import-Module "WebAdministration"

# test & create app pool

Push-Location IIS:\AppPools\

if(!(Test-Path $poolName -PathType Container)){
    New-Item -Path $poolName
}

$pool = Get-Item -Path $poolName

$pool.processModel.identityType = "NetworkService"
$pool.managedRuntimeVersion = "v4.0"
$pool.enable32BitAppOnWin64 = $false
$pool | Set-Item

# grant access to encryptions keys

$cmd = "&'$env:SystemRoot\Microsoft.NET\Framework\v4.0.30319\aspnet_regiis' -pa `"GameWire3Keys`" `"IIS APPPOOL\$poolName`""
Invoke-Expression -Command $cmd

Pop-Location

# test & create app pool

Push-Location IIS:\Sites\

if(!(Test-Path $Name -PathType Container)){
    $iisApp = New-Item $Name -bindings @{protocol="http";bindingInformation="*:80:" + $Name} -physicalPath $sitePath
}

$iisApp = Get-Item -Path $Name
$iisApp | Set-ItemProperty -Name "applicationPool" -Value $poolName
$iisApp | Set-ItemProperty -Name "physicalPath" -Value $sitePath

Pop-Location
#cd $env:SystemDrive

$cmd = "&'$env:programfiles\IIS\Microsoft Web Deploy V3\Scripts\SetupSiteForPublish.ps1' -siteName $Name -deploymentUserName `"$deploymentUserName`" -publishSettingFileName $Name.PublishSettings"
"Cmd:$cmd"

Invoke-Expression -Command $cmd
