﻿$user="Golden154491"
$pass="88golden99"

function CreateBasicAuthentication($username, $password){
    $pair = "$($username):$($password)"
    $bytes = [System.Text.Encoding]::ASCII.GetBytes($pair)
    $base64 = [System.Convert]::ToBase64String($bytes)
    $basicAuthValue = "Basic $base64"
    @{ Authorization = $basicAuthValue }
}

function Withdraw($headers, $account, $amount){
    $body=@{AccountNumber=$account;Amount=$amount}
    Invoke-RestMethod -Uri "https://tegapi.totalegame.net/withdrawal" `
						-Body (ConvertTo-Json $body) `
						-Headers $headers `
						-ContentType "application/json" `
						-Method "POST" `
						| Out-File d:\Temp\createdLog.txt -Append -Width 500
}

$fileContent = Get-Content -Path D:\Temp\accounts.txt

$headers = CreateBasicAuthentication $user $pass

For($i=0; $i -lt $fileContent.Length; $i++){
    $line = $fileContent[$i].Split("`t")
    Withdraw $headers $line[0] $line[1]	
}

