﻿# Build & Deploy script. Author: Leonid Morgenshtern e-Mail: leonidm@spiralsolutions.com
# Requires WDeploySnapin3.0 to be installed and wmsvc handler configured and running on target machine
# Info about WDeploySnapin3.0: http://www.iis.net/learn/publish/using-web-deploy/web-deploy-powershell-cmdlets
# Info about Installing Web Deploy on target machine: http://www.iis.net/downloads/microsoft/web-deploy
# ------------------------------------------------------------------------------------------------------
# This script builds web project (.csproj, .vbproj) and publishes it to web site on destination computer.
# When you execute this script for the first time (e.g. you do not have correct publishsettings file),
# -deployHandle parameter must be supplied (Url to WMSVC handler).
# Also, in this case, you will be asked for credentials.
# After first successfull script execution publishsettings file is created. This file can be used on next deployments.
# NOTE: settings file used by this script can be shared with BackupRestore script and vice versa.
# Since settings file will contain publish credentials encrypted using windows account userkey,
# IT IS IMPORTANT THAT SETTINGS FILE CAN NOT BE SHARED WITH OTHER MACHINES OR USERS. It must be recreated on each machine/account.
# Once publishsettings file created, it can be supplied as parameter to avoid user credentials enter on each script run

param([string]$path = $(Throw "Parameter Missing: -path 'path to project file'"),
	[string]$site = $(Throw "Parameter Missing: -site 'application to upload'"),
	[string]$deployHandle = "",
	[string]$deployHandleUrl = "$deployHandle`?site=$site",
	[string]$configuration = "Release",
	[string]$platform = "AnyCPU",
	[ValidateScript({Test-Path $_})][string]$publishSettings = "",
	[string]$buildLocation = "")

trap{
	Write-Host "Error found!" -ForegroundColor Red
	Write-Host "Variables dump:" -BackgroundColor DarkRed
	Get-Variable -Scope "Script"
	Write-Host "Variables dump end" -BackgroundColor DarkRed
	break
}
function isURIWeb($address) { 
	$uri = $address -as [System.URI] 
	$uri -ne $null -and $uri.AbsoluteURI -ne $null -and $uri.Scheme -match '[http|https]' 
} 

function CreateDestinationSettingsFile($site, $deployHandleUrl){
	$settingsPath = "$env:temp\builds\$site\"
	$settingsFile = "settings.publishsettings"
	$cred = Get-Credential -Credential $env:USERDOMAIN\$env:USERNAME -ErrorAction:Stop
	if((Test-Path -Path "$settingsPath") -ne $true){$t=New-Item -ItemType Directory -Path "$settingsPath"}
	$fileName = "$settingsPath" + ($deployHandleUrl -as [System.URI]).Host + "_" + $cred.UserName.Replace("\",  "_") + ".$settingsFile"
	$tmp = New-WDPublishSettings -Credentials $cred `
							-AgentType "WMSvc" `
							-AllowUntrusted `
							-Site $site `
							-ComputerName $deployHandleUrl `
							-FileName "$fileName" `
							-EncryptPassword
	Write-Host "Settings File:`t$fileName" -ForegroundColor DarkGreen
	"$fileName"
}

$isurl = isURIWeb($deployHandleUrl)
$isSettings = $publishSettings -ne $null -and $publishSettings -ne "" -and (Test-Path -Path "$publishSettings")
if($isurl -ne $true -and $isSettings -ne $true) {Throw "Invalid deploy handler Url: -deployHandle or -deployHandleUrl or valid -publishSettings must be specified"}

if((Get-PSSnapin -Name WDeploySnapin3.0 -ErrorAction SilentlyContinue) -eq $null)
{
	Add-PSSnapin -Name WDeploySnapin3.0
}

# Make build only if needed
if($buildLocation -eq "" -or ((Test-Path -Path "$buildLocation") -ne $true)){
	Write-Host "Starting build..." -ForegroundColor DarkGreen
	$dateFormat = Get-Date -Format "yyyMMddhhmmss"
	$buildLocation = "$env:temp\builds\$site\$configuration\$dateFormat\package.zip"
	$buildCmd = "$env:windir\Microsoft.NET\Framework\v4.0.30319\MSBuild ""$path"" " + `
																		"/T:Package " + `
																		"/P:Configuration=""$configuration"" " + `
																		"/P:Platform=""$platform"" " + `
																		"/P:PackageLocation=""$buildLocation"" " + `
																		"/P:AutoParameterizationWebConfigConnectionStrings=false " + `
																		"/P:DeployIisAppPath=""$site"""

	# Since SolutionDir parameter defined as relative path
	# need to be sure that msbuild executed from the the project folder
	$pObj = Resolve-Path $path
	$cLoc = $pObj.Drive.Root + $pObj.Drive.CurrentLocation
	[Environment]::CurrentDirectory = $cLoc

	Invoke-Expression $buildCmd -ErrorAction Stop #-Verbose 
	Write-Host "Build Finished." -ForegroundColor DarkGreen
}
$destinationSettings = $publishSettings
if($isSettings -ne $true) {
	$destinationSettings = CreateDestinationSettingsFile $site $deployHandleUrl
}

Write-Host "Starting deployment..." -ForegroundColor DarkGreen
Restore-WDPackage -DestinationPublishSettings "$destinationSettings" -Package $buildLocation 
Write-Host "Deployment Finished." -ForegroundColor DarkGreen