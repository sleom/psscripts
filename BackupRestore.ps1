﻿# Backup & Restore script. Author: Leonid Morgenshtern e-Mail: leonidm@spiralsolutions.com
# Requires WDeploySnapin3.0 to be installed and wmsvc handler configured and running on target machine
# Info about WDeploySnapin3.0: http://www.iis.net/learn/publish/using-web-deploy/web-deploy-powershell-cmdlets
# Info about Installing Web Deploy on target machine: http://www.iis.net/downloads/microsoft/web-deploy
# Info about Setup automatic backups: http://www.iis.net/learn/publish/using-web-deploy/web-deploy-automatic-backups
# ------------------------------------------------------------------------------------------------------
# This script roll back site that was previously backed up by automatic webdeploy backup
# When you execute this script for the first time (e.g. you do not have correct publishsettings file),
# -deployHandle parameter must be supplied (Url to WMSVC handler).
# Also, in this case, you will be asked for credentials.
# After first successfull script execution publishsettings file is created.
# NOTE: settings file used by this script can be shared with BuildDeploy script and vice versa.
# Since settings file will contain publish credentials encrypted using windows account userkey,
# IT IS IMPORTANT THAT SETTINGS FILE CAN NOT BE SHARED WITH OTHER MACHINES OR USERS. It must be recreated on each machine/account.
# Once publishsettings file created, it can be supplied as parameter to avoid user credentials enter on each script run


param([ValidateScript({Test-Path $_})][string]$publishSettings = "",
	[ValidateSet('List', 'RestoreLast', 'Restore', 'Sync', 'Dump')]
	[string]$action = 'List',
	[string]$site = "",
	[string]$deployHandle = "",
	[string]$deployHandleUrl = "$deployHandle`?site=$site",
	[string]$backupFile = "",
	[ValidateScript({Test-Path $_})][string]$destPublishSettings = ""
)

trap{
	Write-Host "Error found" -ForegroundColor Red
	break
}

function isURIWeb($address) { 
	$uri = $address -as [System.URI] 
	$uri -ne $null -and $uri.AbsoluteURI -ne $null -and $uri.Scheme -match '[http|https]' 
}

function CreateDestinationSettingsFile($site, $deployHandleUrl){
	$settingsPath = "$env:temp\builds\$site\"
	$settingsFile = "settings.publishsettings"
	$cred = Get-Credential -Credential $env:USERDOMAIN\$env:USERNAME -ErrorAction:Stop
	$fileName = "$settingsPath" + $site + ".$settingsFile"
	Write-Host "Settings File:`t$fileName" -ForegroundColor DarkGreen
	$tmp = New-WDPublishSettings -Credentials $cred `
							-AgentType "WMSvc" `
							-AllowUntrusted `
							-Site $site `
							-ComputerName $deployHandleUrl `
							-FileName "$fileName" `
							-EncryptPassword
	$cred
}

function ParseSettings($settingsFilePath){
	if((Test-Path -Path "$settingsFilePath") -ne $true) {Throw "Invalid settings pass"} 
	$settings = Get-WDPublishSettings $settingsFilePath
	$passwordBytes = [System.Convert]::FromBase64String($settings.Password)
	$unencrytpedData = [System.Security.Cryptography.ProtectedData]::Unprotect($passwordBytes, $null, 'CurrentUser')
	$passwordPlain = [System.Text.Encoding]::Unicode.GetString($unencrytpedData)
	$passwordSecString = ConvertTo-SecureString -String $passwordPlain -AsPlainText -Force
	New-Object System.Management.Automation.PSCredential($settings.Username, $passwordSecString)
	$settings.SiteName
	$settings.PublishUrl
}

Add-Type -AssemblyName System.Security

if((Get-PSSnapin -Name WDeploySnapin3.0 -ErrorAction SilentlyContinue) -eq $null)
{
	Add-PSSnapin -Name WDeploySnapin3.0
}

$isSettings = $publishSettings -ne $null -and $publishSettings -ne "" -and (Test-Path -Path "$publishSettings")
$cred = $null
if($isSettings -eq $true){
	$result = ParseSettings $publishSettings
	$cred = $result[0]
	$site = $result[1]
	$deployHandleUrl = $result[2]
} else {
	$isurl = isURIWeb($deployHandleUrl)
	if($isurl -ne $true -and $isSettings -ne $true) {Throw "Invalid deploy handler Url: -deployHandle or -deployHandleUrl or valid -publishSettings must be specified"}
	$cred = CreateDestinationSettingsFile $site
}
$username = $cred.UserName
$password = $cred.GetNetworkCredential().Password

switch ($action){
	"List" {$verb = "dump"; $source = "backupManager=$site,wmsvc=$deployHandleUrl,username=$username,password=$password"}
	"RestoreLast" {$verb = "sync"; $source = "backupManager"; $dest = "backupManager=$site,wmsvc=$deployHandleUrl,username=$username,password=$password,useLatest=true"}
	"Restore" {$verb = "sync"; $source = "backupManager"; $dest = "backupManager=$site/$backupFile,wmsvc=$deployHandleUrl,username=$username,password=$password"}
	"Sync" {Sync-WDApp -SourcePublishSettings $publishSettings -DestinationPublishSettings $destPublishSettings; return;}
	"Dump" {Backup-WDSite -SourcePublishSettings $publishSettings; return;}
}

$sourcep = "-source=$source"
$exe = "$env:programfiles\IIS\Microsoft Web Deploy V3\msdeploy.exe"
$verbp = "-verb=$verb"
if($dest -ne $null){$destp = "-dest=$dest"} else {$destp = ""}
&$exe $verbp $sourcep $destp -allowUntrusted
